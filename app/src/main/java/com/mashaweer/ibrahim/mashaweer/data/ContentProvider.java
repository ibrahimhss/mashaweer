package com.mashaweer.ibrahim.mashaweer.data;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import static com.mashaweer.ibrahim.mashaweer.data.MashweerContract.MashweerEntry.TABLE_CAR_KIND;
import static com.mashaweer.ibrahim.mashaweer.data.MashweerContract.MashweerEntry.TABLE_FACE;

/**
 * Created by ibrahim on 09/03/18.
 */

public class ContentProvider extends android.content.ContentProvider {
    public static final int CAR_KIND = 100;
    public static final int CAR_KIND_WITH_ID = 101;

    public static final int FACE = 102;
    public static final int FACE_WITH_ID = 103;
    private static final UriMatcher sUriMatcher = buildUriMatcher ();
    private MDbHelber mMDbHelber;

    public static UriMatcher buildUriMatcher () {

        // Initialize a UriMatcher with no matches by passing in NO_MATCH to the constructor
        UriMatcher uriMatcher = new UriMatcher (UriMatcher.NO_MATCH);

        /*
          All paths added to the UriMatcher have a corresponding int.
          For each kind of uri you may want to access, add the corresponding match with addURI.
          The two calls below add matches for the task directory and a single item by ID.
         */
        uriMatcher.addURI (MashweerContract.AUTHORITY, MashweerContract.PATH_CAR_KIND, CAR_KIND);
        uriMatcher.addURI (MashweerContract.AUTHORITY, MashweerContract.PATH_CAR_KIND + "/#", CAR_KIND_WITH_ID);

        uriMatcher.addURI (MashweerContract.AUTHORITY, MashweerContract.PATH_FACE, FACE);
        uriMatcher.addURI (MashweerContract.AUTHORITY, MashweerContract.PATH_FACE + "/#", FACE_WITH_ID);


        return uriMatcher;
    }

    @Override
    public boolean onCreate () {
        Context context = getContext ();
        mMDbHelber = new MDbHelber (context);
        return true;
    }

    @Nullable
    @Override
    public Cursor query (@NonNull Uri uri, String[] projection, String selection,
                         String[] selectionArgs, String sortOrder) {

        // Get access to underlying database (read-only for query)
        final SQLiteDatabase db = mMDbHelber.getReadableDatabase ();

        int match = sUriMatcher.match (uri);
        Cursor retCursor;

        switch (match) {
            case CAR_KIND:
                retCursor = db.query (TABLE_CAR_KIND,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            case FACE:
                retCursor = db.query (TABLE_FACE,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            // Default exception
            default:
                throw new UnsupportedOperationException ("Unknown uri: " + uri);
        }

        // Set a notification URI on the Cursor and return that Cursor
        retCursor.setNotificationUri (getContext ().getContentResolver (), uri);

        // Return the desired Cursor
        return retCursor;
    }

    @Nullable
    @Override
    public String getType (@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert (@NonNull Uri uri, @Nullable ContentValues contentValues) {
        final SQLiteDatabase db = mMDbHelber.getWritableDatabase ();

        // Write URI matching code to identify the match for the tasks directory
        int match = sUriMatcher.match (uri);
        Uri returnUri; // URI to be returned

        switch (match) {
            case CAR_KIND:
                // Insert new values into the database
                // Inserting values into tasks table
                long idCar = db.insert (TABLE_CAR_KIND, null, contentValues);
                if (idCar > 0) {
                    returnUri = ContentUris.withAppendedId (MashweerContract.MashweerEntry.CONTENT_CAR_KIND_URI, idCar);
                } else {
                    throw new android.database.SQLException ("Failed to insert row into  CAR_KIND" + uri);
                }
                break;
            // Set the value for the returnedUri and write the default case for unknown URI's
            // Default case throws an UnsupportedOperationException

            case FACE:
                // Insert new values into the database
                // Inserting values into tasks table
                long idFace = db.insert (TABLE_FACE, null, contentValues);
                if (idFace > 0) {
                    returnUri = ContentUris.withAppendedId (MashweerContract.MashweerEntry.CONTENT_FACE_URI, idFace);
                } else {
                    throw new android.database.SQLException ("Failed to insert row into  CAR_KIND" + uri);
                }
                break;
            // Set the value for the returnedUri and write the default case for unknown URI's
            // Default case throws an UnsupportedOperationException
            default:
                throw new UnsupportedOperationException ("Unknown  uri: " + uri);
        }

        // Notify the resolver if the uri has been changed, and return the newly inserted URI
        getContext ().getContentResolver ().notifyChange (uri, null);

        // Return constructed uri (this points to the newly inserted row of data)
        return returnUri;
    }

    @Override
    public int delete (@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }

    @Override
    public int update (@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }
}
