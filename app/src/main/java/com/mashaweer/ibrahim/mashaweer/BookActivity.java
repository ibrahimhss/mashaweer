package com.mashaweer.ibrahim.mashaweer;

import android.app.Dialog;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mashaweer.ibrahim.mashaweer.dailoge.CustomDateTimePicker;
import com.mashaweer.ibrahim.mashaweer.data.DbGetSpinnerBackend;
import com.mashaweer.ibrahim.mashaweer.data.MDbHelber;
import com.mashaweer.ibrahim.mashaweer.data.SharedPrefManager;
import com.mashaweer.ibrahim.mashaweer.internet.BaseApiService;
import com.mashaweer.ibrahim.mashaweer.internet.UtilsApi;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ibrahim on 10/02/18.
 */

public class BookActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<String[]> {
    private static final String TAG = BookActivity.class.getSimpleName();
    String Car_id, Face_id, sh_id;
    int i = 0;
    TextView txt_fromlocation, txt_tolocation;
    Button btn_Book, et_way, choose_driver;
    CustomDateTimePicker custom;
    BaseApiService mApiService;
    private MDbHelber mDbHelber;
    private TextView txt_label, TxtUserEmail;
    private Spinner mSP_CARKIND, mSP_FACE;
    private CircleImageView imageUser;
    private ArrayList<String> FaceSpFromSqlite, carKndSpFromSqlite;
    private DbGetSpinnerBackend dbGetSpinnerBackend;
    private String selctedCarName, selectedFaceName;
    private MyDrawerLayout mDrawerLayout;
    private String user_id, traveTime, start_latitude, start_longitude, end_latitude, end_longtude, start_location, end_location, status;
    private Cursor mCursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_book );

        mApiService = UtilsApi.getAPIService(); // meng-init yang ada di package apihelper


        txt_fromlocation = findViewById (R.id.txt_fromlocation);
        txt_tolocation = findViewById (R.id.txt_tolocation);
        txt_fromlocation.setText (SharedPrefManager.getInstance (this).getLocationStartUsers ());
        txt_tolocation.setText (SharedPrefManager.getInstance (this).getLocationEndUsers ());
        txt_label= findViewById( R.id.txt_label );
        btn_Book = findViewById (R.id.btn_Book);
        btn_Book = findViewById( R.id.btn_Book );
        choose_driver = findViewById (R.id.choose_driver);

        et_way = findViewById (R.id.et_way);

        mDbHelber = new MDbHelber( this );
      //  Toolbar toolbar = findViewById( R.id.toolbar );

        mSP_CARKIND = findViewById (R.id.SP_CARKIND);
        mSP_FACE = findViewById (R.id.SP_FACE);
        Log.d( TAG, "locationUser: \nend:"+SharedPrefManager.getInstance (this).getLocationEndUsers ());
        Log.d( TAG, "locationUser: \nstart:"+SharedPrefManager.getInstance (this).getLocationStartUsers ());

        dbGetSpinnerBackend = new DbGetSpinnerBackend( this );

        carKndSpFromSqlite = dbGetSpinnerBackend.getcarKindSP();
        carKndSpFromSqlite = dbGetSpinnerBackend.getcarKindSP ();




        FaceSpFromSqlite = dbGetSpinnerBackend.getFaceSP();

        loadSpinnersFromSqlite();
        user_id = SharedPrefManager.getInstance (BookActivity.this).getUserId ();
        traveTime = txt_label.getText ().toString ();
        start_latitude = SharedPrefManager.getInstance (BookActivity.this).getLatitudeStartUsers ();
        start_longitude = SharedPrefManager.getInstance (BookActivity.this).getLongitudeStartUsers ();
        end_latitude = SharedPrefManager.getInstance (BookActivity.this).getLatitudeEndUsers ();
        end_longtude = SharedPrefManager.getInstance (BookActivity.this).getLongitudeEndUsers ();
        start_location = SharedPrefManager.getInstance (BookActivity.this).getLocationStartUsers ();
        end_location = SharedPrefManager.getInstance (BookActivity.this).getLocationEndUsers ();
        status = "yes";
        btn_Book.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               /* if(!forClientsModels.isEmpty()){
                    forClientsModels.clear();
                }*/
                try {
                    Car_id = mDbHelber.getCarKindId( selctedCarName );
                    Face_id =mDbHelber.getFaceId( selectedFaceName );
                    Log.d( TAG, "idDetailsFromSqlite:\nFace_id= " + Face_id +"\nCar_id="+Car_id);

                    if (mSP_CARKIND.getOnItemSelectedListener () == null && mSP_FACE.getOnItemSelectedListener () == null) {
                        Toast.makeText( BookActivity.this.getApplicationContext(), "برجاء اختيار كل العناصر المطلوبه", Toast.LENGTH_LONG ).show();

                    } else if (mSP_FACE.getOnItemSelectedListener () == null) {
                        Toast.makeText( BookActivity.this.getApplicationContext(), "برجاء اختيار فئة الرحله", Toast.LENGTH_LONG ).show();
                    } else if (mSP_CARKIND.getOnItemSelectedListener () == null) {
                        Toast.makeText( BookActivity.this.getApplicationContext(), "برجاء اختيار نوع السياره", Toast.LENGTH_LONG ).show();

                    } else {
                        requestBooking();
                        requestMessage ();
                    }

                } catch (CursorIndexOutOfBoundsException e) {
                    e.printStackTrace();
                }

            }
        } );

        custom = new CustomDateTimePicker(this,
                new CustomDateTimePicker.ICustomDateTimeListener() {
                    @Override
                    public void onSet(Dialog dialog, Calendar calendarSelected,
                                      Date dateSelected, int year, String monthFullName,
                                      String monthShortName, int monthNumber, int date,
                                      String weekDayFullName, String weekDayShortName,
                                      int hour24, int hour12, int min, int sec,
                                      String AM_PM) {
                                     txt_label
                                    .setText(calendarSelected
                                        .get(Calendar.DAY_OF_MONTH)
                                        + "/" + (monthNumber+1) + "/" + year
                                        + ", " + hour12 + ":" + min
                                        + " " + AM_PM);
                    }

                    @Override
                    public void onCancel() {

                    }
                });
        /**
         * Pass Directly current time format it will return AM and PM if you set
         * false
         */
        custom.set24HourFormat(false);
        /**
         * Pass Directly current data and time to show when it pop up
         */
        custom.setDate(Calendar.getInstance());

        findViewById(R.id.button_date).setOnClickListener(
                new View.OnClickListener () {

                    @Override
                    public void onClick(View v) {
                        custom.showDialog();
                    }
                });

    }

    private void loadSpinnersFromSqlite() {


        final ArrayAdapter<String> spinnerAdapterFaceSp = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_dropdown_item, FaceSpFromSqlite ) {
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView( position, convertView, parent );
                TextView tv = (TextView) view;
              /*  tv.setTextColor(  Color.parseColor( "#FFFFFF" )  );

                if (position == 0) {
                    tv.setTextColor(  Color.parseColor( "#FFFFFF" )  );
                }
              if (position % 2 == 1 ) {

                    tv.setBackgroundColor( Color.parseColor( "#FF00D977" ) );
                } else {
                    tv.setBackgroundColor( Color.parseColor( "#FF76F2BA" ) );
                }*/
                return view;
            }
        };
        spinnerAdapterFaceSp.setDropDownViewResource( R.layout.spinner_dropdown_item );
        mSP_FACE.setAdapter (spinnerAdapterFaceSp);

        mSP_FACE.setOnItemSelectedListener (new AdapterView.OnItemSelectedListener () {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedFaceName = (String) parent.getItemAtPosition( position );
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        } );

        final ArrayAdapter<String> spinnerAdaptercarKndSp = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_dropdown_item, carKndSpFromSqlite ) {
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView( position, convertView, parent );
                TextView tv = (TextView) view;
                // tv.setTextColor(  Color.parseColor( "#CC1D1D" )  );
                if (position == 0) {
                    tv.setTextColor( Color.parseColor( "#CC1D1D" ) );
                }
                /*if (position % 2 == 1) {

                    tv.setBackgroundColor( Color.parseColor( "#FF00D977" ) );

                } else {
                    tv.setBackgroundColor( Color.parseColor( "#FF76F2BA" ) );
                }*/
                return view;
            }
        };
        spinnerAdaptercarKndSp.setDropDownViewResource( R.layout.spinner_dropdown_item );
        mSP_CARKIND.setAdapter (spinnerAdaptercarKndSp);

        mSP_CARKIND.setOnItemSelectedListener (new AdapterView.OnItemSelectedListener () {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selctedCarName = (String) parent.getItemAtPosition( position );

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                return;

            }
        } );

    }



//user_id,face_id,car_id,traveTime,start_latitude,start_longitude,end_latitude,end_longtude,start_location,end_location,status
private void requestBooking(){

   String allValue= "\nuser_id="+user_id+"\nface_id="+Face_id+"\ncar_id="+Car_id+"\ntraveTime="+traveTime+"\nstart_latitude="+start_latitude
    +"\nstart_longitude="+start_longitude+"\nend_latitude="+end_latitude+"\nend_longtude="+end_longtude+"\nstart_location="
            +start_location+"\nend_location="+end_location+"\nstatus="+status;
    Log.d("allValueToserver", allValue);

    mApiService.bookRequest (user_id,Face_id,Car_id  ,traveTime,start_latitude,start_longitude,end_latitude,end_longtude,start_location,end_location,status)
            .enqueue(new Callback<ResponseBody> () {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()){
                        Log.i("response", "onResponse:start ");

                        try {
                            String remoteResponse=response.body().string();

                            Log.d("JSON", remoteResponse);

                            JSONObject jsonRESULTS = new JSONObject(remoteResponse);
                            if (jsonRESULTS.getString("response").equals("success")){
                                SharedPrefManager.getInstance( getApplicationContext() ).setLoginDriver(true);

                                Toast.makeText(BookActivity.this, jsonRESULTS.getString("response"), Toast.LENGTH_SHORT).show();

                            } else {
                                // Jika login gagal
                                Log.e("response", "onFailure: ERROR > " );
                                String error_message = jsonRESULTS.getString("failed");

                                Log.e("response", "noAcount: ERROR > "+error_message );

                                Toast.makeText(BookActivity.this, error_message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e("response", "onFailure: ERROR > " + t.toString());
                    Toast.makeText(BookActivity.this, "خطا بالاتصال بالانترنت", Toast.LENGTH_SHORT).show();
                }
            });
}

    private void requestMessage () {
        String name = SharedPrefManager.getInstance (this).getNamesOfUsers ();

        mApiService.sentMessagToDriverRequest (name, start_location, end_location, name)
                .enqueue (new Callback<ResponseBody> () {
                    @Override
                    public void onResponse (Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful ()) {
                            Log.i ("response", "onResponse:start ");

                            try {
                                String remoteResponse = response.body ().string ();

                                Log.d ("JSON", remoteResponse);

                                JSONObject jsonRESULTS = new JSONObject (remoteResponse);
                                if (jsonRESULTS.getString ("error").equals ("false")) {
                                    SharedPrefManager.getInstance (getApplicationContext ()).setLoginDriver (true);

                                    Toast.makeText (BookActivity.this, jsonRESULTS.getString ("message"), Toast.LENGTH_SHORT).show ();

                                } else {
                                    // Jika login gagal
                                    Log.e ("message", "onFailure: ERROR > ");
                                    String error_message = jsonRESULTS.getString ("message");

                                    Log.e ("message", "noAcount: ERROR > " + error_message);

                                    Toast.makeText (BookActivity.this, error_message, Toast.LENGTH_SHORT).show ();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace ();
                            } catch (IOException e) {
                                e.printStackTrace ();
                            }
                        }
                    }

                    @Override
                    public void onFailure (Call<ResponseBody> call, Throwable t) {
                        Log.e ("response", "onFailure: ERROR > " + t.toString ());
                        Toast.makeText (BookActivity.this, "خطا بالاتصال بالانترنت", Toast.LENGTH_SHORT).show ();
                    }
                });
    }

    @Override
    public Loader<String[]> onCreateLoader (int id, Bundle args) {
        return null;
    }

    @Override
    public void onLoadFinished (Loader<String[]> loader, String[] data) {

    }

    @Override
    public void onLoaderReset (Loader<String[]> loader) {

    }
}

