package com.mashaweer.ibrahim.mashaweer.dailoge;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Button;
import android.widget.TextView;

import com.mashaweer.ibrahim.mashaweer.R;

/**
 * Created by ibrahim on 15/02/18.
 */

public class MessageUserDialoge extends Dialog {

    TextView txt_message_user;
    Button btn_other_option, btn_cancel_order;
    Context c;

    public MessageUserDialoge (@NonNull Context context) {
        super (context);
        this.c = context;
    }

    @Override
    protected void onCreate (final Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.message_user_dialoge);

        btn_other_option = findViewById (R.id.btn_other_option);
        btn_cancel_order = findViewById (R.id.btn_cancel_order);
        txt_message_user = findViewById (R.id.txt_message_user);

        //id_user= PreferenceUtil.getClientId( this.c );

    }

}
