package com.mashaweer.ibrahim.mashaweer.dailoge;

/**
 * Created by ibrahim on 15/02/18.
 */

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

import com.mashaweer.ibrahim.mashaweer.R;
import com.mashaweer.ibrahim.mashaweer.data.SharedPrefManager;

public class messageDriverDialoge extends AppCompatActivity {

    private TextView txt_message_driver, txt_to_driver, txt_from_driver;
    private Button btn_driver_other_option, btn_driver_cancl_order;

    @Override
    protected void onCreate (final Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.message_user_dialoge);

        btn_driver_other_option = findViewById (R.id.btn_driver_other_option);
        btn_driver_cancl_order = findViewById (R.id.btn_driver_cancl_order);
        txt_message_driver = findViewById (R.id.txt_message_driver);
        txt_to_driver = findViewById (R.id.txt_message_user);

        txt_message_driver.setText (SharedPrefManager.getInstance (this).getDriverTitleMessage ());
        txt_to_driver.setText (SharedPrefManager.getInstance (this).getDriverToMessage ());
        txt_from_driver.setText (SharedPrefManager.getInstance (this).getDriverFromMessage ());

    }
}