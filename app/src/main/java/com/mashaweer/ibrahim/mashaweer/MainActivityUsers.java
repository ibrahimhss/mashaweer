package com.mashaweer.ibrahim.mashaweer;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mashaweer.ibrahim.mashaweer.data.MDbHelber;
import com.mashaweer.ibrahim.mashaweer.data.SharedPrefManager;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivityUsers extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = MainActivityUsers.class.getSimpleName();

   MDbHelber mDbHelber;
    Button btn_Book;
    private TextView txtName, TxtUserEmail;
    private CircleImageView imageUser;
    private MyDrawerLayout mDrawerLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main_users );

        mDbHelber=new MDbHelber (this);

        btn_Book = findViewById( R.id.btn_Book );

        Toolbar toolbar = findViewById( R.id.toolbar );
        //  getApplicationContext().setSupportActionBar( toolbar );

        DrawerLayout drawer = findViewById( R.id.drawer_layout );
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close );
        drawer.addDrawerListener( toggle );
        toggle.syncState();

        NavigationView navigationView = findViewById( R.id.nav_view );
        navigationView.setNavigationItemSelectedListener( this );

        View headerView = navigationView.getHeaderView( 0 );
        imageUser = headerView.findViewById( R.id.imageUser );
        txtName = headerView.findViewById( R.id.NaveTxtName );
        TxtUserEmail = headerView.findViewById( R.id.NaveTxtEmail );
        Glide.with( MainActivityUsers.this ).load( SharedPrefManager.getInstance( MainActivityUsers.this ).getImageOfUsers() ).into( imageUser );
        txtName.setText( SharedPrefManager.getInstance( this ).getNamesOfUsers() );
        TxtUserEmail.setText( SharedPrefManager.getInstance( this ).getEmailOfUsers() );

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getWindow ().setAttributes (params);

        btn_Book.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(MainActivityUsers.this, MapUserActivity.class);
                startActivity(intent);

            }
        } );


    }



    private void logoutUser() {

        SharedPrefManager.getInstance( this ).setLoginUser( false );
        mDbHelber.deleteUsers();

        // Launching the login activity
        Intent intent = new Intent( MainActivityUsers.this, Home.class );
        startActivity( intent );
        finish();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById (R.id.drawer_layout);
        if (drawer.isDrawerOpen( GravityCompat.START )) {
            drawer.closeDrawer( GravityCompat.START );
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_signout) {
            logoutUser();
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_profile) {

        }


        DrawerLayout drawer = findViewById (R.id.drawer_layout);
        drawer.closeDrawer( GravityCompat.START );
        return true;
    }

}

